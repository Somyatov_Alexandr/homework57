const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontendTaskTotalTime = tasks.filter(task => task.category === 'Frontend').reduce((acc, task) => acc += task.timeSpent, 0);
const bugTaskTotalTime = tasks.filter(task => task.type === 'bug').reduce((acc, task) => acc += task.timeSpent, 0);
const uiTaskCount = tasks.filter(task => task.title.indexOf('UI') !== -1).length;

const frontendTaskCount = tasks.reduce((acc, task) => {
  if (!acc[task.category]){
    acc[task.category] = 1;
  } else {
    acc[task.category]++;
  }
  return acc;
}, {});

const timeMoreThan = tasks.filter(task => task.timeSpent > 4).map(task => {
 return {title: task.title, category: task.category};
});

console.log('Общее количество времени, затраченное на работу над задачами из категории "Frontend" = ' + frontendTaskTotalTime);
console.log('Общее количество времени, затраченное на работу над задачами типа "bug" = ' + bugTaskTotalTime);
console.log('Количество задач, имеющих в названии слово "UI" = ' + uiTaskCount);
console.log('===============================');
console.log('Количество задач каждой категории:');
console.log(frontendTaskCount);
console.log('===============================');
console.log('Массив задач с затраченным временем больше 4 часов:');
console.log(timeMoreThan);
