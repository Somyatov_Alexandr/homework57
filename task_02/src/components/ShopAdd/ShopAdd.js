import React from 'react';

const ShopAdd = props => {
  return (
      <div className="shop__add">
        <form onSubmit={(event) => props.handleSubmit(event)}>
          <input
              onChange={event => props.handleShopName(event)}
              type="text"
              placeholder="Item name"
              id="shopName"
              className="add__control"
          />
          <input type="number"
                 onChange={event => props.handleCost(event)}
                 placeholder="cost"
                 id="shopCost"
                 className="add__control"
          />
          <span className="shop__currency">KGS</span>
          <button type="submit" className="shop__buy">Add</button>
        </form>
      </div>
  );
};

export default ShopAdd;