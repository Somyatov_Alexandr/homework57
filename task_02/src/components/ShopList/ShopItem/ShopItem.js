import React from 'react';

const ShopItem = props => {
  return(
      <div className="shop__item">
        <div className="item__name">{props.title}</div>
        <div className="item__count">x{props.count}</div>
        <div className="item__price">{props.price} <span>KGS</span></div>
        <div className="item__remove" onClick={props.remove}>x</div>
      </div>
  );
};

export default ShopItem;