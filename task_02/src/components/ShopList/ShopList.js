import React from 'react';
import ShopItem from "./ShopItem/ShopItem";
import TotalSpend from "./TotalSpend/TotalSpend";

const ShopList = props => {
  return (
      <div className="shop__info">
        <div className="shop__list">
          {props.shopList.length === 0 ? 'Нет товара' :
            props.shopList.map((item, index) => {
            return <ShopItem
                key={item.title + index}
                title={item.title}
                count={item.count}
                price={item.price}
                remove={() => props.removeShopItem(item.title)}/>
          })}
        </div>
        {props.total !== 0 ? <TotalSpend total={props.total} /> : null}
      </div>
);
};

export default ShopList;