import React from 'react';

const TotalSpend = props => {
  return (
      <div className="total">
        <div className="total__title">Total spend:</div>
        <div className="total__price"><span>{props.total}</span> KGS</div>
      </div>
  );
};

export default TotalSpend;