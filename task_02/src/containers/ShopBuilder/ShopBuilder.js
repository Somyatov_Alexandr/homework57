import React, { Component } from 'react';
import './ShopBuilder.css';
import ShopList from "../../components/ShopList/ShopList";
import ShopAdd from "../../components/ShopAdd/ShopAdd";


class App extends Component {
  state = {
    shops: [],
    total: 0
  };

  shopAdded = {};

  handleSubmit = (event) => {
    event.preventDefault();

    let shopCopy = [...this.state.shops];
    const index = shopCopy.findIndex(item => item.title === this.shopAdded.title);

    if (index === -1){
      let shopItem = {
        title: this.shopAdded.title,
        price: this.shopAdded.price,
        count: 1
      };
      shopCopy.push(shopItem);
    } else {
      let shopItemCopy = {...shopCopy[index]};
      shopItemCopy.count++;
      shopItemCopy.price += this.shopAdded.price;
      shopCopy[index] = shopItemCopy;
    }

    const total = shopCopy.reduce((acc, shop) => {
      return acc + shop.price;
    },0);

    console.log(total);

    this.setState({shops: shopCopy, total: total});
    event.target.reset();
  };

  handleShopName = event => {
    this.shopAdded['title'] = event.target.value;
  };

  handleCost = event => {
      this.shopAdded['price'] = parseInt(event.target.value, 10);
  };


  removeShopItem = title => {
    let shopCopy = [...this.state.shops];
    const index = shopCopy.findIndex(shop => shop.title === title);
    if (index !== -1) {
      shopCopy.splice(index, 1);
    }

    const total = shopCopy.reduce((acc, shop) => {
      return acc + shop.price;
    },0);

    this.setState({shops: shopCopy, total: total});
  };

  render() {
    return (
      <div className="App shop">
        <ShopAdd
            handleSubmit={this.handleSubmit}
            handleShopName={this.handleShopName}
            handleCost={this.handleCost}
        />
        <ShopList
            shopList={this.state.shops}
            total={this.state.total}
            removeShopItem={this.removeShopItem}
        />
      </div>
    );
  };

}

export default App;
