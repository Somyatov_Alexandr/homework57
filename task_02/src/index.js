import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/ShopBuilder/ShopBuilder';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
